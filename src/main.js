import Vue from "vue";
import router from "./router";
import App from "./App.vue";
import store from "./store";
import $ from "jquery";
import "./scss/global.scss";
//import { current } from "fibers";

Vue.config.productionTip = false;

new Vue({
	router: router,
	store,
	render: (h) => h(App),
}).$mount("#app");

// Event Cursor
$(document)
	.on("mouseenter", "a, button", function() {
		$(".cursor").addClass("zooming");
	})
	.on("mouseleave", "a, button", function() {
		$(".cursor").removeClass("zooming");
	});

$(document)
	.on("mouseenter", ".me_title", function() {
		$(".cursor").addClass("zooming_xxl");
		$(".me_more")
			.stop()
			.fadeIn();
	})
	.on("mouseleave", ".me_title", function() {
		$(".cursor").removeClass("zooming_xxl");
		$(".me_more")
			.stop()
			.fadeOut();
	});
