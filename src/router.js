import Vue from "vue";
import VueRouter from "vue-router";
import MeTitle from "./pages/home/HomeMeTitle";
import WhoIAm from "./pages/home/HomeWhoIAm";
import Contact from "./pages/contact/Contact";
import ContactLine from "./components/ContactLine";
import Parcours from "./pages/Parcours";

Vue.use(VueRouter);

export default new VueRouter({
	mode: "history",
	routes: [
		{
			path: "/",
			components: {
				default: MeTitle,
				hommWho: WhoIAm,
				contactLine: ContactLine,
			},
		},
		{
			path: "/contact",
			component: Contact,
		},
		{
			path: "/parcours",
			component: Parcours,
		},
		{
			path: "*",
			component: MeTitle,
		},
	],
});
